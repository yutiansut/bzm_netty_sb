package com.fjb.service.im.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fjb.mapper.im.ImChatFriendInfoMapper;
import com.fjb.pojo.im.ImChatFriendInfo;
import com.fjb.pojo.im.vo.ImChatFriendInfoVo;
import com.fjb.service.im.ImChatFriendInfoService;

/**
 * <p>
 * im_chat 朋友信息表 服务实现类
 * </p>
 *
 * @author hemiao
 * @since 2020-06-02
 */
@Service
public class ImChatFriendInfoServiceImpl extends ServiceImpl<ImChatFriendInfoMapper, ImChatFriendInfo> implements ImChatFriendInfoService {
	
	@Autowired
	private ImChatFriendInfoMapper imChatFriendInfoMapper;
	
	@Override
	public List<ImChatFriendInfoVo> selectFriendList(Integer userId) {
		return imChatFriendInfoMapper.selectFriendList(userId);
	}

}
