package com.fjb.tool.zookeeper.lock;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.Watcher.Event.KeeperState;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;

/**
 * @Description:最简单分布式锁实现方案
 * @author hemiao
 * @time:2020年4月23日 下午10:30:24
 */
public class ZookerSession implements Watcher{
	
	private CountDownLatch countDownLatch=new CountDownLatch(1);
   
	private ZooKeeper zooKeeper;
	
	private static  class  Singleton{
        private  static  ZookerSession instance;
        static {
            instance = new ZookerSession();
        }
 
        public static ZookerSession getInstance(){
            return instance;
        }
    }
    
	public static ZookerSession getInstance(){
        return Singleton.getInstance();
    }
	
	 public ZookerSession() {
	        try {
	            this.zooKeeper = new ZooKeeper("192.168.0.10:2181",50000,this);
	            countDownLatch.await();
	            System.out.println("state:  "+zooKeeper.getState());
	            System.out.println("connection estalished!!");
	        } catch (IOException e) {
	            e.printStackTrace();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	    }
	    public ZooKeeper getZooKeeper(){return this.zooKeeper;}
	    @Override
	    public void process(WatchedEvent event) {
	        if(event.getState()==KeeperState.SyncConnected){
	            countDownLatch.countDown();
	        }
	    }
	    public void releaseDistributeLock(Long id){
	        String path ="/lock_"+id;
	        try {
	            zooKeeper.delete(path,-1);
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        } catch (KeeperException e) {
	            e.printStackTrace();
	        }
	    }
	 
	    public void acquireDistributeLock(Long id){
	        String path ="/lock_"+id;
	        try {
	            zooKeeper.create(path,null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
//	            System.out.println("success to acquire lock "+id);
	        } catch (KeeperException e) {
	            //没有获取到锁
	            int count=0;
	            while (true){
	                try{
	                    Thread.sleep(200);
	                    zooKeeper.create(path,null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
	                }catch (InterruptedException e1) {
	                    count++;
	                    continue;
	                } catch (KeeperException e1) {
//	                    e1.printStackTrace();
	                    count++;
	                    continue;
	                }
//	                System.out.println("success to acquire lock "+id);
	                break;
	            }
	 
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	    }
	 
	
}
